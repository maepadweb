#!/usr/bin/python
#
# This file is part of MaePadWeb
# Copyright (c) 2010 Thomas Perl <thp.io/about>
# http://thp.io/2010/maepad/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
os.chdir(os.path.dirname(__file__) or '.')

import sys
sys.path.insert(0, os.path.dirname(__file__) or '.')

import minidb
import urlparse
import cgi
import random
import re
import string
import subprocess
import BaseHTTPServer
import threading
import gtk
import gobject

try:
    import json
except:
    import simplejson as json

import gconf

class MaePad(object):
    PIXMAPS = '/usr/share/pixmaps/maepad/'
    THEME_BASE = '/etc/hildon/theme/images/'
    THEME = {
            'toolbar_hi': THEME_BASE + 'toolbar_button_pressed.png',
            'toolbar_bg': THEME_BASE + 'ToolbarPrimaryBackground.png',
            'title_bg': THEME_BASE + 'wmTitleBar.png',
            'title_btn': THEME_BASE + 'wmBackIcon.png',
    }
    TOOLBAR_HEIGHT = 70
    TITLE_HEIGHT = 56

    # The data of a fully transparent 1x1 GIF
    NIX_GIF = 'R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='.decode('base64')

    class Node(object):
        UNKNOWN, TEXT, SKETCH, CHECKLIST = range(4)

    class NodeFlag(object):
        NONE = 0
        SKETCHLINES, SKETCHGRAPH, WORDRAP = (1 << x for x in range(3))

    class ChecklistStyle(object):
        CHECKED, BOLD, STRIKE = (1 << x for x in range(3))

    class checklists(object):
        __slots__ = {
            'idx': int,
            'nodeid': int,
            'name': str,
            'style': int,
            'color': int,
            'ord': int,
        }

        @classmethod
        def sorted(cls, l):
            c = lambda a, b: cmp((a.ord, a.idx), (b.ord, b.idx))
            return sorted(l, c)


    class nodes(object):
        __slots__ = {
            'nodeid': int,
            'parent': int,
            'bodytype': int,
            'name': str,
            'body': str,
            'nameblob': str,
            'bodyblob': str,
            'lastmodified': int,
            'ord': int,
            'flags': int,
        }

        @classmethod
        def sorted(cls, l):
            c = lambda a, b: cmp(a.ord, b.ord)
            return sorted(l, c)


class MaePadServer(BaseHTTPServer.BaseHTTPRequestHandler):
    db = None
    close = False
    password = None

    def start_output(self, status=200, content_type='text/html'):
        self.send_response(status)
        self.send_header('Content-type', content_type)
        if status == 401:
            self.send_header('WWW-Authenticate', 'Basic realm="MaePadWeb"')
        self.end_headers()

    def send_as(self, data, mimetype):
        self.start_output(content_type=mimetype)
        self.wfile.write(data)
        self.wfile.close()

    send_html = lambda s, d: s.send_as(d, 'text/html')
    send_js = lambda s, d: s.send_as(d, 'text/javascript')
    send_json = lambda s, d: s.send_as(json.dumps(d), 'text/plain')
    send_png = lambda s, d: s.send_as(d, 'image/png')
    send_gif = lambda s, d: s.send_as(d, 'image/gif')

    def _nodelist__json(self):
        def get_nodes(parent=0):
            return [(x.nodeid, x.name, x.bodytype, get_nodes(x.nodeid)) \
                    for x in self.db.load(MaePad.nodes, parent=parent)]
        self.send_json(get_nodes())

    def _checklist__json(self, id):
        c = lambda x: (x.idx, x.name, x.style, '%06x' % x.color)
        self.send_json([c(x) for x in self.db.load(MaePad.checklists, nodeid=id)])

    def _checklist__setBold(self, id, bold):
        bold = (bold == 'true')
        element = self.db.get(MaePad.checklists, idx=id)
        if bold:
            newStyle = element.style | MaePad.ChecklistStyle.BOLD
        else:
            newStyle = element.style & ~(MaePad.ChecklistStyle.BOLD)
        self.db.update(element, style=newStyle)
        self.send_json(True)

    def _checklist__setStrike(self, id, strike):
        strike = (strike == 'true')
        element = self.db.get(MaePad.checklists, idx=id)
        if strike:
            newStyle = element.style | MaePad.ChecklistStyle.STRIKE
        else:
            newStyle = element.style & ~(MaePad.ChecklistStyle.STRIKE)
        self.db.update(element, style=newStyle)
        self.send_json(True)

    def _checklist__setCheck(self, id, check):
        check = (check == 'true')
        element = self.db.get(MaePad.checklists, idx=id)
        if check:
            newStyle = element.style | MaePad.ChecklistStyle.CHECKED
        else:
            newStyle = element.style & ~(MaePad.ChecklistStyle.CHECKED)
        self.db.update(element, style=newStyle)
        self.send_json(True)

    def _checklist__setText(self, id, text):
        element = self.db.get(MaePad.checklists, idx=id)
        self.db.update(element, name=text)
        self.send_json(True)

    def _checklist__addItem(self, id, text):
        print repr(id)
        checklist = MaePad.checklists()
        checklist.nodeid = int(id)
        checklist.name = text
        checklist.style = 0
        checklist.color = 0
        checklist.ord = 0
        self.db.save(checklist)
        self.send_json(True)

    def _nix__gif(self):
        self.send_gif(MaePad.NIX_GIF)

    def _checklist__deleteItem(self, id):
        self.db.delete(MaePad.checklists, idx=id)
        self.send_json(True)

    def _sketch__png(self, id):
        self.send_png(self.db.get(MaePad.nodes, nodeid=id).bodyblob)

    def _richtext__html(self, id):
        self.send_html(self.db.get(MaePad.nodes, nodeid=id).bodyblob)

    def send_icon(self, icon_name):
        theme = gtk.icon_theme_get_default()
        icon = theme.lookup_icon(icon_name, 32, 0)
        if icon is None:
            filename = os.path.join(MaePad.PIXMAPS,\
                    icon_name+'.png')
        else:
            filename = icon.get_filename()

        self.send_png(open(filename, 'rb').read())

    def send_theme(self, theme_part):
        if theme_part in MaePad.THEME:
            self.send_png(open(MaePad.THEME[theme_part], 'rb').read())

    def is_valid_auth(self, auth):
        if not auth:
            return False
        if not auth.startswith('Basic '):
            return False
        try:
            auth = auth[len('Basic '):].decode('base64')
        except Exception, e:
            return False

        username, password = auth.split(':', 1)
        return (password == MaePadServer.password)

    def client_is_local(self):
        host, port = self.client_address
        return host.startswith('127.')

    def do_GET(self):
        url = urlparse.urlparse(self.path)
        query = cgi.parse_qs(url.query)
        query = dict((x, y[-1]) for x, y in query.items())
        path = filter(None, url.path.split('/'))

        if len(path) == 0:
            mode = 'index'
        elif len(path) == 1:
            mode = path[0]
        elif len(path) == 2 and path[0] == 'icons':
            return self.send_icon(path[1])
        elif len(path) == 2 and path[0] == 'theme':
            return self.send_theme(path[1])
        else:
            self.start_output(404, 'text/plain')
            self.wfile.write("404'd!")
            self.wfile.close()

        if MaePadServer.password and not self.client_is_local():
            auth = self.headers.get('Authorization', '')
            if not self.is_valid_auth(auth):
                self.start_output(401, 'text/plain')
                self.wfile.write('please authenticate.')
                self.wfile.close()
                return

        if mode == 'index':
            self.send_html(open('index.html').read())
        elif mode == 'jquery.js':
            self.send_js(open('jquery-1.4.3.min.js').read())
        elif mode == 'logo.png':
            self.send_png(open('logo.png').read())
        else:
            attr_name = '_' + mode.replace('.', '__')
            if hasattr(self, attr_name):
                getattr(self, attr_name)(**query)

client = gconf.client_get_default()
database_file = client.get_string('/apps/maepad/general/lastdocument') or 'memos.db'

MaePadServer.db = minidb.Store(database_file)
PORT = 8888

def server_thread_proc():
    server = BaseHTTPServer.HTTPServer(('', PORT), MaePadServer)
    server.timeout = 1

    try:
        while not MaePadServer.close:
            server.handle_request()
    except:
        pass

def start_server():
    t = threading.Thread(target=server_thread_proc)
    t.setDaemon(True)
    t.start()

def get_ips():
    p = subprocess.Popen(['/sbin/ifconfig'], stdout=subprocess.PIPE)
    stdout, stderr = p.communicate()
    p.wait()
    return [ip for ip, rest in re.findall(r'addr:((\d+\.){3}\d+)', stdout) if not ip.startswith('127.')]

w = gtk.Window()
w.set_title('MaePadWeb Server')

vb = gtk.VBox()
vb.set_border_width(10)

try:
    import hildon
    sw = hildon.PannableArea()
except Exception, e:
    print >>sys.stderr, e
    sw = gtk.ScrolledWindow()

w.add(sw)
sw.add_with_viewport(vb)

def make_button(title, value=None):
    try:
        import hildon
        b = hildon.Button(gtk.HILDON_SIZE_THUMB_HEIGHT | gtk.HILDON_SIZE_AUTO_WIDTH, 0)
        b.set_title(title)
        if value:
            b.set_value(value)
        return b
    except Exception, e:
        return gtk.Button(title)

def generate_password():
    # http://stackoverflow.com/questions/3854692
    chars = string.letters + string.digits
    length = 6
    return ''.join(random.choice(chars) for _ in xrange(length))


def open_website(url):
    import osso
    context = osso.Context('MaePadWeb', '1.0', False)
    rpc = osso.Rpc(context)
    rpc.rpc_run_with_defaults('osso_browser', \
                              'open_new_window', \
                              (url,))

def on_start_server_clicked(button):
    for child in vb.get_children():
        vb.remove(child)

    # Generate a random password
    MaePadServer.password = generate_password()

    start_server()
    vb.add(gtk.Label('Server running on port %d. Close window to stop server.' % PORT))
    vb.add(gtk.Label('DB file: ' + database_file))
    vb.add(gtk.Label('Your IP(s): ' + ', '.join(get_ips())))
    ip = '127.0.0.1'
    url = 'http://%s:%s/' % (ip, PORT)
    b = make_button('Open in web browser', url)
    b.connect('clicked', lambda b, url: open_website(url), url)
    vb.pack_start(b, expand=False)
    l = gtk.Label()
    l.set_markup('Username: <i>any</i>, Password: <b><tt>%s</tt></b>' % MaePadServer.password)
    vb.add(l)
    vb.show_all()

b = make_button('Start server', 'Start web server on port %d' % PORT)
b.connect('clicked', on_start_server_clicked)

vb.pack_start(b, expand=False)
vb.pack_start(gtk.Label(''))
vb.pack_start(gtk.Label('The service is not encrypted. For use in trusted networks only.'))
vb.pack_start(gtk.Label('Do not run MaePad and MaePadWeb simultaneously.'))

def on_destroy(w):
    MaePadServer.close = True
    gtk.main_quit()

w.connect('destroy', on_destroy)
w.show_all()

gobject.threads_init()

try:
    gtk.main()
except Exception, e:
    print >>sys.stderr, 'Caught exception; closing ('+str(e)+')'

MaePadServer.db.commit()

